# mini-project6: Instrument a Rust Lambda Function with Logging and Tracing
> Xingyu, Zhang (NetID: xz365)


## Rust Lambda Functionality
The basic function remains the same with the previous mini-project5: save the information of a person (i.e.{```name```: string, ```age```: u8}) which was passed to the API to the database. In addtion, it will provide a response of what piece of information was added (i.e. {Inserted person record: ```name```, ```age``` years old"}). In order to better debug and monitor the function, logging and tracing sections are added.


### Add Logging To The Lambda Function
1. Add the ```tracing``` library and the ```tracing-subscriber``` library in [Cargo.toml](./ManagerWithLog/Cargo.toml)
```toml
tracing-subscriber = "0.3.18"
tracing = "0.1.40"
```

2. Apply code to produce and trace loggings in [main.rs](./ManagerWithLog/src/main.rs)
```rust
// Add libraries
use tracing::{info, Level};
use tracing_subscriber::FmtSubscriber;
```
- Initialize logging
```rust
#[tokio::main]
async fn main() -> Result<(), Error> {
    // Initialize logging
    let subscriber = FmtSubscriber::builder()
        .with_max_level(Level::INFO)
        .finish();
    
    tracing::subscriber::set_global_default(subscriber).expect("Unable to set global default");

    info!("Starting the ManagerWithLog service");

    let func = service_fn(handler);
    lambda_runtime::run(func).await?;
    Ok(())
}
```
- Track the error
```rust
// An example in handler function, more can be found in main.rs
let event: Event = serde_json::from_value(_event.payload)
        .map_err(|e| {
            tracing::error!("Error deserializing event to Request: {:?}", e); 
            Error::from(e.to_string())
        })?;
```

### Integrate AWS X-RAY Tracing
First, build and deploy the completed lambda function using the same ```IAM Role``` of mini-project5 which has already attached the permissions of ```AWSLambdaBasicExecutionRole```, ```IAMFullAccess```, ```AWSLambda_FullAccess```, and ```AmazonDynamoDBFullAccess```.
```bash
cargo lambda build --release
cargo lambda deploy --iam-role arn:aws:iam::211125444211:role/service-role/manager-role-5c2g4nqi
```

Then in ```IAM``` section, attach more permissions include ```AWSXrayFullAccess```, ```AWSXRayDaemonWriteAccess``` to enable the X-Ray tracing for the function.

![permissions](./assets/permissions.png)

### Connect Logs/Traces To CloudWatch
In the ```Lambda``` section, enable additional monitoring tools.

![monitor tool](./assets/monitor.png)

### Examine The Log
Two inputs are used to invoke the function:
- One is correct formatted:
```json
{
  "name": "Jackson",
  "age": 26
}
```
![right case](./assets/right_test_case.png)
- Another one is incorrect:
```json
{
  "age": 26
}
```
![error case](./assets/error_test_case.png)

#### CloudWatch
__Logs__
In the log groups, we can find the log streams:

![](./assets/loggroups.png)


Specifically, in the detail of the wrong case's log, we can find the ```Error``` information:

![](./assets/error_log.png)

__X-Ray traces__

![trace](./assets/xRay_traces.png)
